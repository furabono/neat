#include "Species.h"
#include "SpeciesParameter.h"
#include <algorithm>

using namespace NEAT;

bool genomeComp(Genome *f, Genome *l)
{
	return f->fitness > l->fitness;
}

Species::Species(const SpeciesParameter &speciesParameter) 
	: age(0), bestFitnessImprovedAge(0), bestFitnessEver(0), adjustedFitness(0), speciesParameter(speciesParameter)
{
	organisms.clear();
}

Species::Species(const SpeciesParameter &speciesParameter, Genome *genome) : Species(speciesParameter)
{
	organisms.push_back(genome);
}

Genome* Species::operator[](size_t index) const
{
	return organisms[index];
}

size_t Species::size() const
{
	return organisms.size();
}

bool Species::isEmpty() const
{
	return organisms.empty();
}

void Species::addGenome(Genome *genome)
{
	organisms.push_back(genome);
}

void Species::removeOldGenomes()
{
	auto begin = organisms.begin() + ((organisms.size() * speciesParameter.surviveRate));
	for (auto it = begin; it < organisms.end(); ++it)
	{
		delete *it;
	}
	organisms.erase(begin, organisms.end());
}

void Species::nextGeneration()
{
	++age;

	std::sort(organisms.begin(), organisms.end(), genomeComp);

	if (organisms[0]->fitness > bestFitnessEver) 
	{ 
		bestFitnessEver = organisms[0]->fitness;
		bestFitnessImprovedAge = age;
	}

	adjustedFitness = 0;
	for (Genome *genome : organisms)
	{
		adjustedFitness += genome->fitness;
	}
	adjustedFitness /= organisms.size();

	if (age - bestFitnessImprovedAge >= speciesParameter.stagnantAgePenaltyThreshold)
	{
		adjustedFitness *= speciesParameter.stagnantAgePenalty;
	}
	if (age >= speciesParameter.oldAgePenaltyThreshold)
	{
		adjustedFitness *= speciesParameter.oldAgePenalty;
	}
	else if (age <= speciesParameter.youngAgeBonusThreshold)
	{
		adjustedFitness *= speciesParameter.youngAgeBonus;
	}
}

int Species::getAge() const
{
	return age;
}

int Species::getBestFitenssImporvedAge() const
{
	return bestFitnessImprovedAge;
}

double Species::getAdjustedFitness() const
{
	return adjustedFitness;
}

/*
Species::iterator& Species::begin()
{
	return organisms.begin();
}

Species::iterator& Species::end()
{
	return organisms.end();
}
*/