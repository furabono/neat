#include "Neuron.h"
#include "Link.h"

#include <cmath>

#define RESPONSE (0.2)

using namespace NEAT;

double sigmoid(double value)
{
	return 1.0 / (1 + std::exp(-value / RESPONSE));
}

void Neuron::addInput(Link *input)
{
	inputs.push_back(input);
}

void Neuron::removeInput(Link *input)
{

}

void Neuron::addOutput(Link *output)
{
	outputs.push_back(output);
}

void Neuron::removeOutput(Link *output)
{

}

void Neuron::act()
{
	double netInput = 0;
	for (Link *input : inputs) { netInput += input->getValue(); }

	netInput = sigmoid(netInput);

	for (Link *output : outputs) { output->setValue(netInput); }
}