#pragma once

namespace NEAT
{
	class Link
	{
	public:
		Link(double weight);

		void setValue(double value);
		double getValue();

	private:
		double weight;
		double value;

	};
}