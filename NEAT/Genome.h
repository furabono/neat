#pragma once
#include "Gene.h"
#include "Random.h"
#include <vector>

namespace NEAT
{
	struct Genome
	{
		using iterator = std::vector<Gene>::iterator;
		using const_iterator = std::vector<Gene>::const_iterator;

		Genome() = default;
		Genome(int numInputs, int numOutputs);
		//Genome(Genome &genome);

		//bool findGene(int input, int output);
		double calculateDepthBetween(int input, int output);

		std::vector<Gene> genes;
		std::vector<std::pair<int, double>> neurons;	// first : neuron number, second : depth
		double fitness;
		//int id;
	};
}