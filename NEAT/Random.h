#pragma once
#include <random>

namespace NEAT
{
	class Random
	{
	private:
		Random();
		static Random& getInstance();

	public:
		static int randomInt(int min, int max);
		static double randomReal(double min, double max);

	private:
		std::mt19937 engine;

	};
}