#pragma once
#include "LinkInfo.h"

namespace NEAT
{
	struct Gene
	{
		Gene() = default;
		Gene(const Gene &gene) :innovationNumber(gene.innovationNumber), linkInfo(gene.linkInfo), enabled(gene.enabled) {}

		int innovationNumber;
		LinkInfo linkInfo;
		bool enabled;
	};
}