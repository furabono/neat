#pragma once
#include "Genome.h"

namespace NEAT
{
	struct SpeciesParameter;

	class Species
	{
	public:
		using iterator = std::vector<Genome*>::iterator;

	private:
		Species(Species &copy) = delete;
		Species(Species &&move) = delete;

	public:
		Species(const SpeciesParameter &speciesParameter);
		Species(const SpeciesParameter &speciesParameter, Genome *genome);

		Genome* operator[](size_t index) const;
		//iterator& begin();
		//iterator& end();
		size_t size() const;
		bool isEmpty() const;

		void addGenome(Genome *genome);
		//void removeGenome(Genome *genome);
		void removeOldGenomes();

		void nextGeneration();

		int getAge() const;
		int getBestFitenssImporvedAge() const;
		double getAdjustedFitness() const;

	private:
		std::vector<Genome*> organisms;

		int age;
		double bestFitnessEver;
		int bestFitnessImprovedAge;
		double adjustedFitness;

		const SpeciesParameter &speciesParameter;
	};
}