#pragma once

namespace NEAT
{
	struct LinkInfo
	{
		int inputNeuron;
		int outputNeuron;
		double weight;

		LinkInfo() = default;
		LinkInfo(int inputNeuron, int outputNeuron, double weight)
			:inputNeuron(inputNeuron), outputNeuron(outputNeuron), weight(weight) {}
	};
}