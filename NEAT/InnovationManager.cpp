#include "InnovationManager.h"
#include "Gene.h"

using namespace NEAT;
using namespace std;

InnovationManager::InnovationManager(int numInputs, int numOutputs, int innovationLifespan)
	: innovationLifespan(innovationLifespan), newInnovationNumber(0), newNeuronNumber(numInputs + numOutputs)
{

	NewLinkInnovation newLinkInnovation;

	for (int i = 0; i < numInputs; ++i)
	{
		for (int j = 0; j < numOutputs; ++j)
		{
			newLinkInnovation.innovationNumber = ++newInnovationNumber;
			newLinkInnovation.life = innovationLifespan + 1;
			newLinkInnovations.insert(make_pair(make_pair(i + 1, numInputs + j + 1), newLinkInnovation));
		}
	}
}

int InnovationManager::newLink(int inputNeuron, int outputNeuron)
{
	auto key = pair<int, int>(inputNeuron, outputNeuron);
	auto innovation = newLinkInnovations.find(key);
	
	if (innovation == newLinkInnovations.end())
	{
		NewLinkInnovation newLinkInnovation;
		newLinkInnovation.innovationNumber = ++newInnovationNumber;
		newLinkInnovation.life = innovationLifespan;
		newLinkInnovations.insert(make_pair(key, newLinkInnovation));
		return newInnovationNumber;
	}
	else
	{
		return innovation->second.innovationNumber;
	}
}

pair<int, int> InnovationManager::newNeuron(Gene &splitGene)
{
	auto innovation = newNeuronInnovations.find(splitGene.innovationNumber);

	if (innovation == newNeuronInnovations.end())
	{
		NewNeuronInnovation newNeuronInnovation;
		newNeuronInnovation.innovationNumber = ++newInnovationNumber; ++newInnovationNumber;
		newNeuronInnovation.life = innovationLifespan;
		newNeuronInnovation.neuronNumber = ++newNeuronNumber;
		newNeuronInnovation.splitedInnovationNumber = splitGene.innovationNumber;
		newNeuronInnovations.insert(make_pair(splitGene.innovationNumber, newNeuronInnovation));
		return pair<int, int>(newNeuronInnovation.innovationNumber, newNeuronNumber);
	}
	else
	{
		return pair<int, int>(innovation->second.innovationNumber, innovation->second.neuronNumber);
	}
}

void InnovationManager::nextGeneration()
{
	if (innovationLifespan < 1) return;

	auto newLinkInnovation = newLinkInnovations.begin();
	auto newLinkEnd = newLinkInnovations.end();
	for (; newLinkInnovation != newLinkEnd;)
	{
		if (--newLinkInnovation->second.life == 0)
		{
			newLinkInnovation = newLinkInnovations.erase(newLinkInnovation);
			continue;
		}
		++newLinkInnovation;
	}

	auto newNeuronInnovation = newNeuronInnovations.begin();
	auto newNeuronEnd = newNeuronInnovations.end();
	for (; newNeuronInnovation != newNeuronEnd;)
	{
		if (--newNeuronInnovation->second.life == 0)
		{
			newNeuronInnovation = newNeuronInnovations.erase(newNeuronInnovation);
			continue;
		}
		++newNeuronInnovation;
	}
}