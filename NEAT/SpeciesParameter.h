#pragma once

namespace NEAT
{
	struct SpeciesParameter
	{
		int youngAgeBonusThreshold;
		double youngAgeBonus;
		int oldAgePenaltyThreshold;
		double oldAgePenalty;
		int stagnantAgePenaltyThreshold;
		double stagnantAgePenalty;

		double surviveRate;
		double reproduceRate;
	};
}