#include "GeneticAlgorithm.h"
#include "Random.h"

#include <set>
#include <map>
#include <tuple>
#include <cassert>

#define ADJUST_DISTANCE_THRESHOLD (0.1)

using namespace NEAT;
using namespace std;

GeneticAlgorithm::GeneticAlgorithm(const GeneticAlgorithmParameter &geneticAlgorithmParameter, 
									const function<void(Genome*)> &fitnessTest)
	: innovationManager(geneticAlgorithmParameter.numInputs, 
						geneticAlgorithmParameter.numOutputs, 
						geneticAlgorithmParameter.innovationLifespan),
		fitnessTest(fitnessTest)
{
	populationSize = geneticAlgorithmParameter.populationSize;

	speciesParameter.youngAgeBonusThreshold = geneticAlgorithmParameter.youngAgeBonusThreshold;
	speciesParameter.youngAgeBonus = geneticAlgorithmParameter.youngAgeBonus;
	speciesParameter.oldAgePenaltyThreshold = geneticAlgorithmParameter.oldAgePenaltyThreshold;
	speciesParameter.oldAgePenalty = geneticAlgorithmParameter.oldAgePenalty;
	speciesParameter.stagnantAgePenaltyThreshold = geneticAlgorithmParameter.stagnantAgePenaltyThreshold;
	speciesParameter.stagnantAgePenalty = geneticAlgorithmParameter.stagnantAgePenalty;
	speciesParameter.surviveRate = geneticAlgorithmParameter.surviveRate;
	speciesParameter.reproduceRate = geneticAlgorithmParameter.reproduceRate;

	crossoverRate = geneticAlgorithmParameter.crossoverRate;
	crossoverTrials = geneticAlgorithmParameter.crossoverTrials;

	mutationRate = geneticAlgorithmParameter.mutationRate;
	newNeuronMutationRate = geneticAlgorithmParameter.newNeuronMutationRate;
	newLinkMutationRate = geneticAlgorithmParameter.newLinkMutationRate;
	newLinkTrials = geneticAlgorithmParameter.newLinkTrials;
	weightMutationRate = geneticAlgorithmParameter.weightMutationRate;
	randomWeightMutationRate = geneticAlgorithmParameter.randomWeightMutationRate;
	maxPerturbation = geneticAlgorithmParameter.maxPerturbation;
	geneToggleMutationRate = geneticAlgorithmParameter.geneToggleMutationRate;
	geneToggleTrials = geneticAlgorithmParameter.geneToggleTrials;

	excessCoefficient = geneticAlgorithmParameter.excessCoefficient;
	disjoingCoefficient = geneticAlgorithmParameter.disjoingCoefficient;
	weightDifferenceCoefficient = geneticAlgorithmParameter.weightDifferenceCoefficient;

	maxNumSpecies = geneticAlgorithmParameter.maxNumSpecies;
	distanceThreshold = geneticAlgorithmParameter.distanceThreshold;

	numInputs = geneticAlgorithmParameter.numInputs;
	numOutputs = geneticAlgorithmParameter.numOutputs;
	
	
	for (int i = 0; i < populationSize; ++i)
	{
		reproducedOffsprings.push_back(new Genome(geneticAlgorithmParameter.numInputs, geneticAlgorithmParameter.numOutputs));
	}

	generation = -1;
}

GeneticAlgorithm::~GeneticAlgorithm()
{
	for (Genome *genome : reproducedOffsprings)
	{
		delete genome;
	}
	reproducedOffsprings.clear();

	for (Species *species : specieses)
	{
		for (int i = 0; i < species->size(); ++i)
		{
			delete (*species)[i];
		}
		delete species;
	}
	specieses.clear();
}

bool speciesComp(const Species *f, const Species *l)
{
	return (*f)[0]->fitness > (*l)[0]->fitness;
}

void GeneticAlgorithm::epoch()
{
	++generation;
	innovationManager.nextGeneration();

	double totalAdjustedFitness = 0;
	for (Species *species : specieses)
	{
		species->nextGeneration();

		totalAdjustedFitness += species->getAdjustedFitness();
	}
	totalAdjustedFitness /= populationSize;

	sort(specieses.begin(), specieses.end(), speciesComp);

	int numReproduce;
	int numSurvive;
	int totNumSurvive = 0;

	for (Species *species : specieses)
	{
		numSurvive = species->size() * speciesParameter.surviveRate;
		//numSurvive = numSurvive > 0;
		numReproduce = (species->getAdjustedFitness() / totalAdjustedFitness) - numSurvive;
		totNumSurvive += numSurvive;

		reproduce(*species, numReproduce);
	}

	if (reproducedOffsprings.size() + totNumSurvive < populationSize)
	{
		reproduce(*specieses[0], populationSize - reproducedOffsprings.size() - totNumSurvive);
	}

	for (Species *species : specieses)
	{
		species->removeOldGenomes();
	}

	if (maxNumSpecies >= 1)
	{
		if (specieses.size() < maxNumSpecies)
		{
			distanceThreshold -= ADJUST_DISTANCE_THRESHOLD;
			if (distanceThreshold < ADJUST_DISTANCE_THRESHOLD)
			{
				distanceThreshold = ADJUST_DISTANCE_THRESHOLD;
			}
		}
		else if (specieses.size() > maxNumSpecies)
		{
			distanceThreshold += ADJUST_DISTANCE_THRESHOLD;
		}
	}

	
	for (auto it = specieses.begin(); it != specieses.end();)
	{
		if ((*it)->isEmpty())
		{
			delete (*it);
			it = specieses.erase(it);
			continue;
		}
		++it;
	}

	bool done;
	for (Genome *newOffspring : reproducedOffsprings)
	{
		done = false;
		for (Species *species : specieses)
		{
			if (distance(*newOffspring, *(*species)[0]) < distanceThreshold)
			{
				species->addGenome(newOffspring);
				done = true;
				break;
			}
		}
		if (!done)
		{
			specieses.push_back(new Species(speciesParameter, newOffspring));
		}
	}

	reproducedOffsprings.clear();

	//fitnessTest
	for (Species *species : specieses)
	{
		assert(!species->isEmpty());
		for (int i = 0; i < species->size(); ++i)
		{
			fitnessTest((*species)[i]);
		}
	}
}

void GeneticAlgorithm::reproduce(const Species &species, int numReproduce)
{
	roulette.setSpecies(species, speciesParameter.reproduceRate);

	int parent;
	int crossoverParent;
	Genome *newOffspring;
	int i;
	int j;
	for (i = 0; i < numReproduce; ++i)
	{
		parent = roulette.getRandomGenome();

		if (species.size() > 1 && Random::randomReal(0, 1) < crossoverRate)
		{
			for (j = 0; j < crossoverTrials - 1; ++j)
			{
				crossoverParent = roulette.getRandomGenome();
				if (parent != crossoverParent)
				{
					newOffspring = crossover(*species[parent], *species[crossoverParent]);
					break;
				}
			}
			if (j == crossoverTrials - 1)
			{
				newOffspring = new Genome(*species[parent]);
			}
		}
		else
		{
			newOffspring = new Genome(*species[parent]);
		}

		if (Random::randomReal(0, 1) < mutationRate)
		{
			mutate(newOffspring);
		}

		reproducedOffsprings.push_back(newOffspring);
	}

	//roulette.clear();
}

Genome* GeneticAlgorithm::crossover(const Genome &x, const Genome &y)
{
	for (int i = 0; i < x.genes.size() - 1; ++i)
	{
		assert(x.genes[i].innovationNumber < x.genes[i + 1].innovationNumber);
	}
	for (int i = 0; i < y.genes.size() - 1; ++i)
	{
		assert(y.genes[i].innovationNumber < y.genes[i + 1].innovationNumber);
	}

	Genome *newGenome = new Genome();

	Genome::const_iterator gene[2];
	Genome::const_iterator end[2];
	if (x.fitness > y.fitness)
	{
		gene[0] = x.genes.begin();
		end[0] = x.genes.end();
		gene[1] = y.genes.begin();
		end[1] = y.genes.end();

		newGenome->neurons.assign(x.neurons.begin(), x.neurons.end());
	}
	else
	{
		gene[0] = y.genes.begin();
		end[0] = y.genes.end();
		gene[1] = x.genes.begin();
		end[1] = x.genes.end();

		newGenome->neurons.assign(y.neurons.begin(), y.neurons.end());
	}

	//map<int, tuple<double, int, int>> neurons;
	
	while (gene[0] != end[0] && gene[1] != end[1])
	{
		if (gene[0]->innovationNumber == gene[1]->innovationNumber)
		{
			newGenome->genes.push_back(*gene[Random::randomInt(0, 1)]);
			++gene[0];
			++gene[1];
		}
		else if (gene[0]->innovationNumber < gene[1]->innovationNumber)
		{
			newGenome->genes.push_back(*gene[0]);
			++gene[0];
		}
		else
		{
			++gene[1];
		}
	}

	while (gene[0] != end[0])
	{
		newGenome->genes.push_back(*gene[0]);
		++gene[0];
	}

	//assert(newGenome->neurons.size() == neurons.size());
	for (int i = 0; i < newGenome->genes.size() - 1; ++i)
	{
		assert(newGenome->genes[i].innovationNumber < newGenome->genes[i + 1].innovationNumber);
	}

	return newGenome;
}

void GeneticAlgorithm::mutate(Genome *genome)
{
	if (Random::randomReal(0, 1) < newNeuronMutationRate)
	{
		Gene &splitGene = genome->genes[Random::randomInt(0, genome->genes.size() - 1)];
		std::pair<int, int> innovation = innovationManager.newNeuron(splitGene);
		
		splitGene.enabled = false;
		Gene input(splitGene);
		Gene output(splitGene);

		input.innovationNumber = innovation.first;
		input.linkInfo.weight = 1;
		input.linkInfo.outputNeuron = innovation.second;
		input.enabled = true;

		output.innovationNumber = innovation.first + 1;
		output.linkInfo.inputNeuron = innovation.second;
		output.enabled = true;
		
		genome->genes.push_back(input);
		genome->genes.push_back(output);

		// 주의 : vector에 새로운 요소가 push_back 된 이후 기존의 splitGene 레퍼런스는 유효하지 않다. (iterator과 비슷)

		double depth = genome->calculateDepthBetween(input.linkInfo.inputNeuron, output.linkInfo.outputNeuron);
		genome->neurons.push_back(pair<int, double>(innovation.second, depth));
	}

	if (Random::randomReal(0, 1) < newLinkMutationRate)
	{
		pair<int, double> input;
		pair<int, double> output;
		int neuronsSize = genome->neurons.size();
		bool exist;
		for (int i = 0; i < newLinkTrials; ++i)
		{
			input = genome->neurons[Random::randomInt(0, neuronsSize - 1)];
			output = genome->neurons[Random::randomInt(numInputs, neuronsSize - 1)];

			if (input.first == output.first) continue;
			if (input.second >= output.second) continue;

			exist = false;
			for (Gene &gene : genome->genes)
			{
				if (gene.linkInfo.inputNeuron == input.first && gene.linkInfo.outputNeuron == output.first)
				{
					exist = true;
					break;
				}
			}
			if (exist) continue;

			int innovation = innovationManager.newLink(input.first, output.first);

			Gene newGene;
			newGene.linkInfo.inputNeuron = input.first;
			newGene.linkInfo.outputNeuron = output.first;
			newGene.linkInfo.weight = Random::randomReal(-1, 1);
			newGene.enabled = true;
			newGene.innovationNumber = innovation;

			genome->genes.push_back(newGene);
			break;
		}
	}

	if (Random::randomReal(0, 1) < weightMutationRate)
	{
		Gene &gene = genome->genes[Random::randomInt(0, genome->genes.size() - 1)];
		if (Random::randomReal(0, 1) < randomWeightMutationRate)
		{
			gene.linkInfo.weight = Random::randomReal(-1, 1);
		}
		else
		{
			gene.linkInfo.weight += Random::randomReal(-1, 1) * maxPerturbation;
		}
	}

	if (Random::randomReal(0, 1) < geneToggleMutationRate)
	{
		int genesSize = genome->genes.size();
		int input;
		int output;
		int numInputLinks;
		int numOutputLinks;
		bool done = false;
		for (int i = 0; i < geneToggleTrials; ++i)
		{
			Gene &gene = genome->genes[Random::randomInt(0, genesSize - 1)];
			if (gene.enabled == true)
			{
				input = gene.linkInfo.inputNeuron;
				output = gene.linkInfo.outputNeuron;
				numInputLinks = 0; 
				numOutputLinks = 0;

				for (Gene &g : genome->genes)
				{
					numInputLinks += (g.linkInfo.inputNeuron == input);
					numOutputLinks += (g.linkInfo.outputNeuron == output);
					if (numInputLinks > 1 && numOutputLinks > 1)
					{
						gene.enabled = false;
						done = true;
						break;
					}
				}
			}
			else
			{
				gene.enabled = true;
				done = true;
			}

			if (done) break;
		}
	}
}

double GeneticAlgorithm::distance(const Genome &x, const Genome &y)
{
	int length;

	if (x.genes.size() > y.genes.size()) 
	{ 
		length = x.genes.size(); 
	}
	else
	{
		length = y.genes.size();
	}

	Genome::const_iterator xGene = x.genes.begin();;
	Genome::const_iterator xEnd = x.genes.end();
	Genome::const_iterator yGene = y.genes.begin();
	Genome::const_iterator yEnd = y.genes.end();

	int numExcess;
	int numDisjoint = 0;
	int numWeightDifference = 0;
	double weightDifference = 0;
	while (yGene != yEnd && xGene != xEnd)
	{
		if (xGene->innovationNumber == yGene->innovationNumber)
		{
			++numWeightDifference;
			weightDifference += abs(xGene->linkInfo.weight - yGene->linkInfo.weight);
			++xGene;
			++yGene;
		}
		else if (xGene->innovationNumber < yGene->innovationNumber)
		{
			++numDisjoint;
			++xGene;
		}
		else
		{
			++numDisjoint;
			++yGene;
		}
	}

	numExcess = (xEnd - xGene) + (yEnd - yGene);

	return (excessCoefficient * numExcess * 1.0 / length) +
		(disjoingCoefficient * numDisjoint * 1.0 / length) +
		(weightDifferenceCoefficient * weightDifference / numWeightDifference);
}

int GeneticAlgorithm::getGeneration() const
{
	return generation;
}

const vector<Species*>* GeneticAlgorithm::getSpecieses() const
{
	return &specieses;
}