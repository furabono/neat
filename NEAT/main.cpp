#include <vld.h>

#include <iostream>
#include <string>
#include <conio.h>
#include <iomanip>
#include "Network.h"
#include "NetworkInfo.h"
#include "GeneticAlgorithm.h"

using namespace NEAT;
using namespace std;

vector<Genome*> genomes;

void store(Genome *genome)
{
	genomes.push_back(genome);
}

void fitnessTest(Genome *genome)
{
	NetworkInfo ni;
	ni.numInputs = 3;
	ni.numOutputs = 1;
	
	for (Gene &gene : genome->genes)
	{
		if (gene.enabled)
		{
			ni.linkInfos.push_back(gene.linkInfo);
		}
	}
	ni.neuronDepthInfos.assign(genome->neurons.begin(), genome->neurons.end());

	Network network(ni);

	double fitness = 0;
	vector<double> output;

	int cnt = 0;

	output = network.input({ 0,0,-1 });
	fitness += 1 - output[0];
	//cout << output[0] << " ";
	cnt += output[0] < 0.5;

	output = network.input({ 0,1,-1 });
	fitness += output[0];
	//cout << output[0] << " ";
	cnt += output[0] > 0.5;

	output = network.input({ 1,0,-1 });
	fitness += output[0];
	//cout << output[0] << " ";
	cnt += output[0] > 0.5;

	output = network.input({ 1,1,-1 });
	fitness += 1 - output[0];
	//cout << output[0] << "\n";
	cnt += output[0] < 0.5;

	if (cnt == 4) fitness += 10;

	genome->fitness = fitness;
}

bool genomeComp2(Genome *f, Genome *l)
{
	return f->fitness > l->fitness;
}

void print(int generation, const GeneticAlgorithm &ga)
{
	sort(genomes.begin(), genomes.end(), genomeComp2);

	cout << " <<" << generation << ">>\n";
	for (int i = 0; i < genomes.size() / 5; ++i)
	{
		cout << setw(3)<< i+1 << " : "<< genomes[i]->fitness;
		cout << " | ";
		for (Gene &gene : genomes[i]->genes)
		{
			if (gene.enabled)
			{
				cout << "[" << gene.linkInfo.inputNeuron << "," << gene.linkInfo.outputNeuron << ":" << gene.linkInfo.weight<< "]";
			}
		}
		cout << "\n";
	}
	cout << "\nSpecieses : " << ga.getSpecieses()->size() << "\n";
}

int main()
{
	GeneticAlgorithmParameter gap;
	gap.numInputs						= 3;
	gap.numOutputs						= 1;
	gap.innovationLifespan				= 3;

	gap.populationSize					= 150;

	gap.youngAgeBonusThreshold			= 5;
	gap.youngAgeBonus					= 1;
	gap.oldAgePenaltyThreshold			= 10;
	gap.oldAgePenalty					= 1;
	gap.stagnantAgePenaltyThreshold		= 15;
	gap.stagnantAgePenalty				= 0.01;

	gap.surviveRate						= 0.2;
	gap.reproduceRate					= 0.2;

	gap.crossoverRate					= 0.5;
	gap.crossoverTrials					= 5;

	gap.mutationRate					= 1;
	gap.newNeuronMutationRate			= 0.03;
	gap.newLinkMutationRate				= 0.3;
	gap.newLinkTrials					= 5;
	gap.weightMutationRate				= 0.9;
	gap.randomWeightMutationRate		= 0.1;
	gap.maxPerturbation					= 2.5;
	gap.geneToggleMutationRate			= 0.005;
	gap.geneToggleTrials				= 5;

	gap.excessCoefficient				= 1;
	gap.disjoingCoefficient				= 1;
	gap.weightDifferenceCoefficient		= 0.4;

	gap.maxNumSpecies					= 10;
	gap.distanceThreshold				= 3.0;

	GeneticAlgorithm ga(gap, store);

	int generation = 0;
	char cmd;
	while (1)
	{
		cmd = _getch();
		if (cmd == 'q' || cmd == 'Q') break;
		if (cmd == 'n' || cmd == 'N')
		{
			genomes.clear();
			ga.epoch();
			for (Genome *genome : genomes)
			{
				fitnessTest(genome);
			}

			system("cls");
			print(++generation, ga);
		}
		if (cmd == 'l' || cmd == 'L')
		{
			int loop;
			cin >> loop;
			for (int i = 0; i < loop; ++i)
			{
				genomes.clear();
				ga.epoch();
				++generation;
				for (Genome *genome : genomes)
				{
					fitnessTest(genome);
				}
			}
			system("cls");
			print(generation, ga);
		}
		if (cmd == 'd' || cmd == 'D')
		{

		}
	}

	return 0;
}