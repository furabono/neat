#include "Random.h"
#include <iostream>

using namespace NEAT;
using namespace std;

Random::Random()
{
	random_device rd;
	engine.seed(rd());
}

Random& Random::getInstance()
{
	static Random instance;
	return instance;
}

int Random::randomInt(int min, int max)
{
	uniform_int_distribution<int> dist(min, max);
	return dist(getInstance().engine);
}

double Random::randomReal(double min, double max)
{
	uniform_real_distribution<double> dist(min, max);
	return dist(getInstance().engine);
}