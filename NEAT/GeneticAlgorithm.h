#pragma once
#include "Species.h"
#include "Roulette.h"
#include "InnovationManager.h"
#include "NetworkInfo.h"
#include "SpeciesParameter.h"
#include <vector>
#include <algorithm>
#include <functional>

namespace NEAT
{
	struct GeneticAlgorithmParameter;

	class GeneticAlgorithm
	{
	private:
		GeneticAlgorithm(GeneticAlgorithm &copy) = delete;
		GeneticAlgorithm(GeneticAlgorithm &&move) = delete;

	public:
		GeneticAlgorithm(const GeneticAlgorithmParameter &geneticAlgorithmParameter, const std::function<void(Genome*)> &fitnessTest);
		~GeneticAlgorithm();

		void epoch();

		int getGeneration() const;
		const std::vector<Species*>* getSpecieses() const;

	private:
		void reproduce(const Species &speices, int numReproduce);
		Genome* crossover(const Genome &x, const Genome &y);
		void mutate(Genome *genome);

		double distance(const Genome &x, const Genome &y);

	private:
		int generation;

		std::vector<Species*> specieses;
		InnovationManager innovationManager;
		std::vector<Genome*> reproducedOffsprings;

		std::function<void(Genome*)> fitnessTest;

		int populationSize;
		//int newGenomeID;

		SpeciesParameter speciesParameter;

		double crossoverRate;
		int crossoverTrials;
		//double offspringWithoutCrossoverRate;
		//double interspeciesCrossoverRate;

		double mutationRate;
		double newNeuronMutationRate;
		double newLinkMutationRate;
		int newLinkTrials;
		double weightMutationRate;
		//double uniformlyPerturbedWeightMutationRate;
		double randomWeightMutationRate;
		double maxPerturbation;
		double geneToggleMutationRate;
		int geneToggleTrials;

		double excessCoefficient;
		double disjoingCoefficient;
		double weightDifferenceCoefficient;

		int maxNumSpecies;
		double distanceThreshold;

		Roulette roulette;

		//NetworkInfo networkInfo;
		int numInputs;
		int numOutputs;
	};

	struct GeneticAlgorithmParameter
	{
		int numInputs;
		int numOutputs;
		int innovationLifespan;

		int populationSize;

		int youngAgeBonusThreshold;
		double youngAgeBonus;
		int oldAgePenaltyThreshold;
		double oldAgePenalty;
		int stagnantAgePenaltyThreshold;
		double stagnantAgePenalty;

		double surviveRate;
		double reproduceRate;

		double crossoverRate;
		int crossoverTrials;
		//double offspringWithoutCrossoverRate;
		//double interspeciesCrossoverRate;

		double mutationRate;
		double newNeuronMutationRate;
		double newLinkMutationRate;
		int newLinkTrials;
		double weightMutationRate;
		//double uniformlyPerturbedWeightMutationRate;
		double randomWeightMutationRate;
		double maxPerturbation;
		double geneToggleMutationRate;
		int geneToggleTrials;

		double excessCoefficient;
		double disjoingCoefficient;
		double weightDifferenceCoefficient;

		int maxNumSpecies;
		double distanceThreshold;
	};
}