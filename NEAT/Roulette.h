#pragma once
#include <vector>

namespace NEAT
{
	class Genome;
	class Species;

	class Roulette
	{
	public:
		//void addGenome(Genome *genome);
		void setSpecies(const Species &species, double reproduceRate);
		//void clear();

		int getRandomGenome() const;

	private:
		std::vector<double> pie;
		double sumFitness;

	};
}