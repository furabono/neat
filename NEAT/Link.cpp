#include "Link.h"

using namespace NEAT;

Link::Link(double weight) :weight(weight), value(0) {}

void Link::setValue(double value)
{
	this->value = value * weight;
}

double Link::getValue()
{
	return value;
}