#include "Network.h"
#include "NetworkInfo.h"
#include "Neuron.h"
#include "Link.h"

#include <map>
#include <algorithm>

#include <iostream>
#include <cassert>

using namespace NEAT;
using namespace std;

bool depthComp(pair<int, double> &f, pair<int, double> &l)
{
	if (f.second == l.second)
	{
		return f.first < l.first;
	}
	return f.second < l.second;
}

Network::Network(const NetworkInfo &networkInfo)
{
	int i;
	map<int, Neuron*> createdNeurons;		// first : Neuron Number, second : Neuron Pointer

	// Input, Output Neuron 积己
	for (i = 0; i < networkInfo.numInputs; ++i)
	{
		createdNeurons.insert(make_pair(i + 1, new Neuron()));
	}
	for (i = 0; i < networkInfo.numOutputs; ++i)
	{
		createdNeurons.insert(make_pair(i + networkInfo.numInputs + 1, new Neuron()));
	}

	// Link, Hidden Neuron 积己
	for (const LinkInfo &linkInfo : networkInfo.linkInfos)
	{
		if (createdNeurons.find(linkInfo.inputNeuron) == createdNeurons.end())
		{
			auto result = createdNeurons.insert(pair<int, Neuron*>(linkInfo.inputNeuron, new Neuron()));
			assert(result.second && result.first->first == linkInfo.inputNeuron && result.first->second != nullptr);
		}
		if (createdNeurons.find(linkInfo.outputNeuron) == createdNeurons.end())
		{
			auto result = createdNeurons.insert(pair<int, Neuron*>(linkInfo.outputNeuron, new Neuron()));
			assert(result.second && result.first->first == linkInfo.outputNeuron && result.first->second != nullptr);
		}

		Link *link = new Link(linkInfo.weight);
		createdNeurons[linkInfo.inputNeuron]->addOutput(link);
		createdNeurons[linkInfo.outputNeuron]->addInput(link);
		links.push_back(link);
	}

	assert(createdNeurons.size() <= networkInfo.neuronDepthInfos.size());

	// Neuron 角青 鉴辑 搬沥
	vector<pair<int, double>> neuronDepthsSort(networkInfo.neuronDepthInfos);
	std::sort(neuronDepthsSort.begin(), neuronDepthsSort.end(), depthComp);
	for (pair<int, double> &neuronDepth : neuronDepthsSort)
	{
		if (createdNeurons.find(neuronDepth.first) == createdNeurons.end()) continue;
		neurons.push_back(createdNeurons[neuronDepth.first]);
		//cout << neuronDepth.first << " ";
	}
	//cout << endl;
	createdNeurons.clear();
	neuronDepthsSort.clear();

	Link *input;
	for (i = 0; i < networkInfo.numInputs; ++i)
	{
		input = new Link(1);
		neurons[i]->addInput(input);
		inputs.push_back(input);
	}

	Link *output;
	for (i = neurons.size() - networkInfo.numOutputs; i < neurons.size(); ++i)
	{
		output = new Link(1);
		neurons[i]->addOutput(output);
		outputs.push_back(output);
	}
}

Network::~Network()
{
	for (Neuron *neuron : neurons) { delete neuron; }
	neurons.clear();

	for (Link *link : links) { delete link; }
	links.clear();

	for (Link *input : inputs) { delete input; }
	inputs.clear();

	for (Link *output : outputs) { delete output; }
	outputs.clear();
}

vector<double> Network::input(const vector<double> &inputs)
{
	assert(inputs.size() == this->inputs.size());
	for (int i = 0; i < inputs.size(); ++i)
	{
		this->inputs[i]->setValue(inputs[i]);
	}

	for (Neuron *neuron : neurons)
	{
		neuron->act();
	}

	vector<double> outputs;
	for (Link *output : this->outputs)
	{
		outputs.push_back(output->getValue());
	}
	
	return outputs;
}