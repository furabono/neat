#pragma once
#include <vector>
#include "LinkInfo.h"

namespace NEAT
{
	struct NetworkInfo
	{
		int numInputs;
		int numOutputs;
		std::vector<LinkInfo> linkInfos;
		std::vector<std::pair<int, double>> neuronDepthInfos;
	};
}