#pragma once
#include <map>

namespace NEAT
{
	struct Gene;

	class InnovationManager
	{
	public:
		InnovationManager(int numInputs, int numOutputs, int innovationLifespan);

		int newLink(int inputNeuron, int outputNeuron);
		std::pair<int, int> newNeuron(Gene &splitGene);	// first : innovation number, second : neuron number

		void nextGeneration();

	private:
		struct NewLinkInnovation
		{
			int innovationNumber;
			int life;
		};
		struct NewNeuronInnovation
		{
			int splitedInnovationNumber;
			int neuronNumber;
			int innovationNumber;
			int life;
		};

	private:
		std::map<std::pair<int, int>, NewLinkInnovation> newLinkInnovations;
		std::map<int, NewNeuronInnovation> newNeuronInnovations;

		int innovationLifespan;
		int newInnovationNumber;
		int newNeuronNumber;
	};
}