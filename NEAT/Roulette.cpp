#include "Roulette.h"
#include "Random.h"
#include "Species.h"

using namespace NEAT;
using namespace std;

void Roulette::setSpecies(const Species &species, double reproduceRate)
{
	pie.clear();
	sumFitness = 0;

	int numSelection = species.size() * reproduceRate;
	if (numSelection < 1) { numSelection = 1; }

	for (int i = 0; i < numSelection; ++i)
	{
		sumFitness += species[i]->fitness;
		pie.push_back(species[i]->fitness);
	}
}

int Roulette::getRandomGenome() const
{
	double randomReal = Random::randomReal(0, sumFitness);

	int l = 0;
	int r = pie.size() - 1;
	int m;
	while (l < r)
	{
		m = (l + r) / 2;
		if (randomReal <= pie[m])
		{
			r = m;
		}
		else
		{
			l = m + 1;
		}
	}

	return l;
}