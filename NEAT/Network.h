#pragma once
#include <vector>
#include <map>

namespace NEAT
{
	struct NetworkInfo;
	class Neuron;
	class Link;

	class Network
	{
	public:
		Network(const NetworkInfo &networkInfo);
		~Network();

		std::vector<double> input(const std::vector<double> &inputs);

	private:
		std::vector<Neuron*> neurons;
		std::vector<Link*> links;
		std::vector<Link*> inputs;
		std::vector<Link*> outputs;

	};
}