#include "Genome.h"

using namespace NEAT;
using namespace std;

Genome::Genome(int numInputs, int numOutputs)
{
	int i;
	int j;
	int innovation = 0;
	Gene gene;
	gene.enabled = true;
	for (i = 0; i < numInputs; ++i)
	{
		for (j = 0; j < numOutputs; ++j)
		{
			gene.innovationNumber = ++innovation;
			gene.linkInfo.inputNeuron = i + 1;
			gene.linkInfo.outputNeuron = numInputs + j + 1;
			gene.linkInfo.weight = Random::randomReal(-1, 1);
			genes.push_back(gene);
		}
	}

	for (i = 0; i < numInputs; ++i)
	{
		neurons.push_back(pair<int, double>(i + 1, 0));
	}
	for (i = 0; i < numOutputs; ++i)
	{
		neurons.push_back(pair<int, double>(numInputs + i + 1, 100));
	}
}

double Genome::calculateDepthBetween(int input, int output)
{
	double inputDepth = -1;
	double outputDepth = -1;

	for (pair<int, double> &neuron : neurons)
	{
		if (neuron.first == input) { inputDepth = neuron.second; }
		else if (neuron.first == output) { outputDepth = neuron.second; }

		if (inputDepth != -1 && outputDepth != -1) { break; }
	}

	return (inputDepth + outputDepth) / 2;
}