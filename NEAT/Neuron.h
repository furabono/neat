#pragma once
#include <vector>

namespace NEAT
{
	class Link;

	class Neuron
	{
	public:
		void addInput(Link *input);
		void removeInput(Link *input);
		void addOutput(Link *output);
		void removeOutput(Link *output);

		void act();

	private:
		std::vector<Link*> inputs;
		std::vector<Link*> outputs;

	};
}